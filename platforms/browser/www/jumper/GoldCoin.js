/* Author: Derek O Reilly, Dundalk Institute of Technology, Ireland. */

class GoldCoin extends GameObject {
    /* Each gameObject MUST have a constructor() and a render() method.        */

    /* If the object animates, then it must also have an updateState() method. */

    constructor(image, y, width, height, updateStateMilliseconds, delay = 0) {
        super(updateStateMilliseconds, delay); /* as this class extends from GameObject, you must always call super() */

        this.collectSound = document.createElement('audio');
        this.collectSound.src = 'music/coin_collect.mpg';
        $(this.collectSound).prop('volume', 0.50);
        /* These variables depend on the object */
        this.image = image;
        this.width = width;
        this.height = height;
        this.x = canvas.width;
        this.y = y;
        let rand = Math.floor((Math.random() * 5) + 1);
        this.gameObjectIsDisplayed = (rand == 1);

        this.NUMBER_OF_SPRITES = 10; // the number of gameObjects in the gameObject image
        this.NUMBER_OF_COLUMNS_IN_SPRITE_IMAGE = 1; // the number of columns in the gameObject image
        this.NUMBER_OF_ROWS_IN_SPRITE_IMAGE = 10; // the number of rows in the gameObject image
        this.currentgameObject = 0;

        this.START_ROW = 0;
        this.START_COLUMN = 0;
        this.row = this.START_ROW;
        this.column = this.START_COLUMN;
        this.counter = 0;
        this.collected = false;
        this.shouldMove = true;
    }

    updateState() {
        if (gameObjects[CHARACTER + 2].currentgameObject === gameObjects[CHARACTER + 2].NUMBER_OF_SPRITES) {
            this.shouldMove = false;
        }

        this.counter++;
        if (this.counter >= 20) {
            this.currentgameObject++;
            if (this.currentgameObject === this.NUMBER_OF_SPRITES) {
                this.column = this.START_COLUMN;
                this.row = this.START_ROW;
                this.currentgameObject = 0;
            } else {
                this.column++;
                if (this.column === this.NUMBER_OF_COLUMNS_IN_SPRITE_IMAGE) {
                    this.column = 0;
                    this.row++;
                }
            }
            this.counter = 0;
        }

        if (this.shouldMove) {
            this.x--;
            if (this.x <= -canvas.width) {
                this.x = canvas.width;
                let rand = Math.floor((Math.random() * 5) + 1);
                this.gameObjectIsDisplayed = (rand == 1);
                this.collected = false;
            }
        }

        if (gameObjects[CHARACTER + 1].isDisplayed() && this.isDisplayed()) {
            if (gameObjects[CHARACTER + 1].x >= this.x - (50 / 2) && gameObjects[CHARACTER + 1].x <= this.x + (50 / 2)) {
                this.gameObjectIsDisplayed = false;
                if (!this.collected) {
                    // this.collectSound.currentTime = 0;
                    this.collectSound.play();
                    this.collected = true;
                    COINS_COLLECTED += 10;
                    gameObjects[STATIC_TEXT].text = "COINS: " + COINS_COLLECTED;
                }
            }
        }
    }

    render() {
        if(this.isDisplayed()) {
            let SPRITE_WIDTH = (this.image.width / this.NUMBER_OF_COLUMNS_IN_SPRITE_IMAGE);
            let SPRITE_HEIGHT = (this.image.height / this.NUMBER_OF_ROWS_IN_SPRITE_IMAGE);
            ctx.drawImage(this.image, this.column * SPRITE_WIDTH, this.row * SPRITE_HEIGHT, SPRITE_WIDTH, SPRITE_HEIGHT, this.x, this.y, this.width, this.height);
        }
    }
}