/* Author: Derek O Reilly, Dundalk Institute of Technology, Ireland.             */
/* There should always be a javaScript file with the same name as the html file. */
/* This file always holds the playGame function().                               */
/* It also holds game specific code, which will be different for each game       */

let character_level = 1;
$.ajax({
    method: "GET",
    url: "http://localhost/jumper_game/select_data.php",
    data: {uuid: guid()},
    dataType: "json",
    async:false
}).done(function(response){
    if(response[0]){
        character_level = response[0].character_level;
    }
});

/******************** Declare game specific global data and functions *****************/
/* images must be declared as global, so that they will load before the game starts  */
let backgroundImage = new Image();
backgroundImage.src = "images/parallax saturated background pack/11_background.png";
let distantCouldsImage = new Image();
distantCouldsImage.src = "images/parallax saturated background pack/10_distant_clouds.png";
let distantCouldsImage2 = new Image();
distantCouldsImage2.src = "images/parallax saturated background pack/09_distant_clouds1.png";
let cloudsImage = new Image();
cloudsImage.src = "images/parallax saturated background pack/08_clouds.png";
let hugeClouds = new Image();
hugeClouds.src = "images/parallax saturated background pack/07_huge_clouds.png";
let hillImage = new Image();
hillImage.src = "images/parallax saturated background pack/06_hill2.png";
let hillImage2 = new Image();
hillImage2.src = "images/parallax saturated background pack/05_hill1.png";
let bushesImage = new Image();
bushesImage.src = "images/parallax saturated background pack/04_bushes.png";
let distantTreesImage = new Image();
distantTreesImage.src = "images/parallax saturated background pack/03_distant_trees.png";
let treesImage = new Image();
treesImage.src = "images/parallax saturated background pack/02_trees_and_bushes.png";
let groundImage = new Image();
groundImage.src = "images/parallax saturated background pack/01_ground.png";
////////////////////////////////////////////////
let dinoRun = new Image();
dinoRun.src = "images/dino/dino_run.png";
let dinoJump = new Image();
dinoJump.src = "images/dino/dino_jump.png";
let dinoDead = new Image();
dinoDead.src = "images/dino/dead_dino.png";

let santaRun = new Image();
santaRun.src = "images/santa/run_santa.png";
let santaJump = new Image();
santaJump.src = "images/santa/jump_santa.png";
let santaDead = new Image();
santaDead.src = "images/santa/dead_santa.png";
//////////////////////////////////////////////////
let stoneImage = new Image();
stoneImage.src = "images/_rocks/stone02.png";
let goldCoin = new Image();
goldCoin.src = "images/coins/gold_coin.png";
let bronzeCoin = new Image();
bronzeCoin.src = "images/coins/bronze_coin.png";
let resetButton = new Image();
resetButton.src = "images/menu/restart.png";

/******************* END OF Declare game specific data and functions *****************/

let COIN = 30;
let CHARACTER = 45;
let DYNAMIC_TEXT = 50;
let STATIC_TEXT = 55;
let BUTTON = 60;

let gamePointer = null;

/* However, the content of this function will be different for each game */
function playGame() {
    deployGame();

    /* If they are needed, then include any game-specific mouse and keyboard listners */

    $("#gameCanvas").on('mousedown', function(e){
        if (e.which === 1)  // left mouse button
        {
            let canvasBoundingRectangle = document.getElementById("gameCanvas").getBoundingClientRect();
            let mouseX = e.clientX - canvasBoundingRectangle.left;
            let mouseY = e.clientY - canvasBoundingRectangle.top;

            if (gameObjects[BUTTON].isDisplayed() && gameObjects[BUTTON].pointIsInsideBoundingRectangle(mouseX, mouseY))
            {
                location.reload();
            }

            if (gameObjects[BUTTON+1].isDisplayed() && gameObjects[BUTTON+1].pointIsInsideBoundingRectangle(mouseX, mouseY))
            {
                location.reload();
            }
        }
    });

    $(document).click(function (event) {

        if (!gameObjects[CHARACTER + 2].isDisplayed()) {
            if (gameObjects[CHARACTER + 1].ended) {
                gameObjects[CHARACTER].reset();
                gameObjects[CHARACTER + 1].reset();

                gameObjects[CHARACTER].stopAndHide();
                gameObjects[CHARACTER + 1].start();
            }
        }
    });

    function deployGame() {
        gameObjects.push(new ScrollingBackgroundImage(backgroundImage, 1000));
        gameObjects.push(new ScrollingBackgroundImage(distantCouldsImage, 70 / 2));
        gameObjects.push(new ScrollingBackgroundImage(distantCouldsImage2, 60 / 2));
        gameObjects.push(new ScrollingBackgroundImage(cloudsImage, 30 / 2));
        gameObjects.push(new ScrollingBackgroundImage(hugeClouds, 40 / 2));
        gameObjects.push(new ScrollingBackgroundImage(hillImage, 40 / 2));
        gameObjects.push(new ScrollingBackgroundImage(hillImage2, 40 / 2));
        gameObjects.push(new ScrollingBackgroundImage(bushesImage, 40 / 2));
        gameObjects.push(new ScrollingBackgroundImage(distantTreesImage, 40 / 2));
        gameObjects.push(new ScrollingBackgroundImage(treesImage, 20 / 2));
        // gameObjects.push(new Stone(stoneImage, 15/2));
        gameObjects.push(new ScrollingBackgroundImage(groundImage, 15 / 2));
        gameObjects.push(new Stone(stoneImage, 15 / 2));
        gameObjects.push(new Stone(stoneImage, 15 / 2, 2000));
        gameObjects.push(new Stone(stoneImage, 15 / 2, 5000));
        gameObjects.push(new Stone(stoneImage, 15 / 2, 8000));
        gameObjects.push(new Stone(stoneImage, 15 / 2, 10500));
        gameObjects.push(new Stone(stoneImage, 15 / 2, 13000));
        gameObjects[COIN] = new BronzeCoin(bronzeCoin, 310, 50, 50, 15 / 2, 3000);
        gameObjects[COIN+1] = new BronzeCoin(bronzeCoin, 310, 50, 50, 15 / 2, 5000);
        gameObjects[COIN+2] = new BronzeCoin(bronzeCoin, 310, 50, 50, 15 / 2, 7000);
        gameObjects[COIN+3] = new BronzeCoin(bronzeCoin, 310, 50, 50, 15 / 2, 9000);
        gameObjects[COIN+4] = new BronzeCoin(bronzeCoin, 310, 50, 50, 15 / 2, 11000);
        gameObjects[COIN+5] = new BronzeCoin(bronzeCoin, 310, 50, 50, 15 / 2, 12000);
        gameObjects[COIN+6] = new BronzeCoin(bronzeCoin, 310, 50, 50, 15 / 2, 14000);
        gameObjects[COIN+7] = new BronzeCoin(bronzeCoin, 310, 50, 50, 15 / 2, 16000);
        gameObjects[COIN+8] = new GoldCoin(goldCoin, 310, 50, 50, 15 / 2, 800);
        if(character_level==1) {
            gameObjects[CHARACTER] = new Dino(dinoRun, 350, 680 / 4, 472 / 4, 100, 0);
        }else{
            gameObjects[CHARACTER] = new Santa(santaRun, 350, 680 / 4, 472 / 4, 100, 0);

        }

        /* END OF game specific code. */

        gameObjects[STATIC_TEXT] = new StaticText("[CURRENT] Coins: " + COINS_COLLECTED, 0, 20, 20, "red", 0);
        gameObjects[STATIC_TEXT+1] = new DistanceText("DISTANCE (current):  ", 0, 40, 20, "red", 0);
        gameObjects[BUTTON] = new Button(0, 100, 50, 50, "", false, resetButton);

        /* Always create a game that uses the gameObject array */
        let game = new CanvasGame();
        gamePointer = game;

        game.start();
        gameObjects[BUTTON+1] = new Button(BUTTON_CENTRE, 200, 50, 50, "", false, resetButton);
        if(character_level==1) {
            gameObjects[CHARACTER + 1] = new DinoJump(dinoJump, 350, 680 / 4, 472 / 4, 150, 0);
            gameObjects[CHARACTER + 2] = new DeadDino(dinoDead, 350, 680 / 4, 472 / 4, 150, 0);
        }else{
            gameObjects[CHARACTER + 1] = new SantaJump(santaJump, 350, 680 / 4, 472 / 4, 150, 0);
            gameObjects[CHARACTER + 2] = new DeadSanta(santaDead, 350, 680 / 4, 472 / 4, 150, 0);

        }
        $.ajax({
            method: "GET",
            url: "http://localhost/jumper_game/select_data.php",
            data: {uuid: guid()},
            dataType: "json"
        }).done(function(response){
            let dist = 0;
            let coins =0;
            let character_level = 1;
            if(response[0]){
                dist = response[0].distance;
                coins = response[0].coins;
                character_level = response[0].character_level;
            }
            coins = character_level*300-coins;

            gameObjects[STATIC_TEXT+2] = new StaticText("Highest Distance: "+dist, 0, 60, 20, "red", 0);
            gameObjects[STATIC_TEXT+3] = new StaticText("Coins needed for next character: "+coins, 0, 80, 20, "red", 0);
            gameObjects[STATIC_TEXT+2].start();
            gameObjects[STATIC_TEXT+3].start();
        });
    }
    /* If they are needed, then include any game-specific mouse and keyboard listners */
}