/* Author: Derek O Reilly, Dundalk Institute of Technology, Ireland. */

class SantaJump extends GameObject {
    constructor(image, y, width, height, updateStateMilliseconds, delay = 0) {
        super(updateStateMilliseconds, delay); /* as this class extends from GameObject, you must always call super() */

        this.jumpSound = document.createElement('audio');
        this.jumpSound.src = 'music/jump.mp3';
        $(this.jumpSound).prop('volume', 0.20);
        /* These variables depend on the object */
        this.defaultY = y;
        this.image = image;
        this.width = width;
        this.height = height;
        this.x = 100;
        this.y = y;
        this.ended = true;

        this.NUMBER_OF_SPRITES = 8; // the number of gameObjects in the gameObject image
        this.NUMBER_OF_COLUMNS_IN_SPRITE_IMAGE = 8; // the number of columns in the gameObject image
        this.NUMBER_OF_ROWS_IN_SPRITE_IMAGE = 1; // the number of rows in the gameObject image
        this.currentgameObject = 0;

        this.START_ROW = 0;
        this.START_COLUMN = 0;
        this.row = this.START_ROW;
        this.column = this.START_COLUMN;

        this.isFirstCallOfUpdateState = true;
    }

    updateState() {
        if (this.isFirstCallOfUpdateState)
        {
            this.jumpSound.currentTime = 0;
            this.jumpSound.play();
            this.isFirstCallOfUpdateState = false;
        }
        if(gameObjects[CHARACTER+2].isDisplayed()){
            this.stopAndHide();
        }
        this.currentgameObject++;
        if (this.currentgameObject === this.NUMBER_OF_SPRITES) {
            gameObjects[CHARACTER].start();
            gameObjects[CHARACTER+1].stopAndHide();
            this.ended = true;
            this.column = this.START_COLUMN;
            this.row = this.START_ROW;
            this.currentgameObject = 0;
            this.isFirstCallOfUpdateState = true;
        } else {
            this.ended = false;
            this.column++;
            if (this.column === this.NUMBER_OF_COLUMNS_IN_SPRITE_IMAGE) {
                this.column = 0;
                this.row++;
            }
            if (this.currentgameObject > 1 && this.currentgameObject < 5) {
                this.y -= 20;
            } else if (this.currentgameObject > 4 && this.currentgameObject < 8) {
                this.y += 20;
            }
        }
    }

    render() {
        if (this.gameObjectIsDisplayed) {
            let SPRITE_WIDTH = (this.image.width / this.NUMBER_OF_COLUMNS_IN_SPRITE_IMAGE);
            let SPRITE_HEIGHT = (this.image.height / this.NUMBER_OF_ROWS_IN_SPRITE_IMAGE);
            ctx.drawImage(this.image, this.column * SPRITE_WIDTH, this.row * SPRITE_HEIGHT, SPRITE_WIDTH, SPRITE_HEIGHT, this.x, this.y, this.width, this.height);
        }
    }

    reset() {
        this.column = this.START_COLUMN;
        this.row = this.START_ROW;
        this.currentgameObject = 0;
        this.y = this.defaultY;
    }
}