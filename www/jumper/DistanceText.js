class DistanceText extends StaticText {
    constructor(text, x, y, fontSize, colour) {
        super(text + DISTANCE_TRAVELLED, x, y, fontSize, colour);
        this.defaultText = text;
        this.stopCount = false;
    }

    render() {
        if (!this.stopCount) {
            DISTANCE_TRAVELLED++;
            this.text = this.defaultText + DISTANCE_TRAVELLED;
        }
        StaticText.prototype.render.call(this);
    }

    stop() {
        super.stop();
        this.stopCount = true;
    }
}