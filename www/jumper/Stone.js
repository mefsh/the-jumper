class Stone extends GameObject {
    constructor(image, updateStateMilliseconds, delay = 0) {
        super(updateStateMilliseconds, delay); /* as this class extends from GameObject, you must always call super() */

        /* These variables depend on the object */
        this.image = image;

        this.x = canvas.width;
    }

    updateState() {
        this.x--;
        if (this.x > 100 - (this.image.width/2-5) && this.x < 100 + (this.image.width/2)) {
            if (gameObjects[CHARACTER].isDisplayed()) {
                gameObjects[CHARACTER].stopAndHide();
                gameObjects[CHARACTER + 2].start();
            }
        }

        if (this.x <= -canvas.width) {
            this.x = canvas.width;
        }

        if (gameObjects[CHARACTER + 2].currentgameObject === gameObjects[CHARACTER + 2].NUMBER_OF_SPRITES ){
            this.stop();
        }
    }

    render() {
        ctx.drawImage(this.image, this.x, 340, this.image.width, this.image.height);
    }
}