/* Author: Derek O Reilly, Dundalk Institute of Technology, Ireland. */

class DeadSanta extends GameObject {
    constructor(image, y, width, height, updateStateMilliseconds, delay = 0) {
        super(updateStateMilliseconds, delay); /* as this class extends from GameObject, you must always call super() */

        this.gameOver = document.createElement('audio');
        this.gameOver.src = 'music/misc/Female/Meghan Christian/miscellaneous_1_meghan.wav';
        $(this.gameOver).prop('volume', 0.20);

        this.hitDmg = document.createElement('audio');
        this.hitDmg.src = 'music/dmg.wav';
        $(this.hitDmg).prop('volume', 0.50);
        /* These variables depend on the object */
        this.image = image;
        this.width = width;
        this.height = height;
        this.x = 100;
        this.y = y;

        this.NUMBER_OF_SPRITES = 8; // the number of gameObjects in the gameObject image
        this.NUMBER_OF_COLUMNS_IN_SPRITE_IMAGE = 8; // the number of columns in the gameObject image
        this.NUMBER_OF_ROWS_IN_SPRITE_IMAGE = 1; // the number of rows in the gameObject image
        this.currentgameObject = 0;

        this.START_ROW = 0;
        this.START_COLUMN = 0;
        this.row = this.START_ROW;
        this.column = this.START_COLUMN;
    }

    updateState() {
        this.currentgameObject++;
        if (this.currentgameObject === this.NUMBER_OF_SPRITES) {
            $.ajax({
                method: "GET",
                url: "http://localhost/jumper_game/insert_or_update.php",
                data: {
                    uuid: guid(),
                    distance: DISTANCE_TRAVELLED,
                    coins: COINS_COLLECTED
                },
                dataType: "json"
            });
            for (let i = 5; i < gameObjects.length; i++) {
                if (i >= COIN && i < CHARACTER) {
                    continue;
                }
                gameObjects[i].stop();
            }
            gameObjects[DYNAMIC_TEXT].start();
            gameObjects[DYNAMIC_TEXT + 1].start();
            gameObjects[CHARACTER].stopAndHide();
            gameObjects[CHARACTER + 1].stopAndHide();
            this.gameOver.play();
            gameObjects[BUTTON].start();
        } else {
            gameObjects[DYNAMIC_TEXT] = new StaticText("GAME OVER", 350,70,  60, "red", 0);
            gameObjects[DYNAMIC_TEXT + 1] = new StaticText("Collected coins: " + COINS_COLLECTED, 300,150,60, "red", 0);
            this.hitDmg.play();
            gameObjects[CHARACTER].stopAndHide();
            gameObjects[CHARACTER + 1].stopAndHide();
            this.column++;
            if (this.column === this.NUMBER_OF_COLUMNS_IN_SPRITE_IMAGE) {
                this.column = 0;
                this.row++;
            }
        }
    }

    render() {
        if (this.gameObjectIsDisplayed) {
            let SPRITE_WIDTH = (this.image.width / this.NUMBER_OF_COLUMNS_IN_SPRITE_IMAGE);
            let SPRITE_HEIGHT = (this.image.height / this.NUMBER_OF_ROWS_IN_SPRITE_IMAGE);
            ctx.drawImage(this.image, this.column * SPRITE_WIDTH, this.row * SPRITE_HEIGHT, SPRITE_WIDTH, SPRITE_HEIGHT, this.x, this.y, this.width, this.height);
        }
    }

    reset() {
        this.column = this.START_COLUMN;
        this.row = this.START_ROW;
        this.currentgameObject = 0;
    }
}